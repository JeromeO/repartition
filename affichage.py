from decimal import Decimal


def css_sytle():
    t = "  <style>\n"
    t += "    table, th, td {\n    border: 1px solid black;\n    border-collapse: collapse;\n  }\n"
    t += "    th, td {\n    padding: 5px;\n  }\n"
    t += "    td {\n    text-align: center;\n    width: 70px;\n  }\n"
    t += "    .nom {\n    text-align: left;\n    width: 170px;\n  }\n"
    t += "    table {\n    border-spacing: 5px;\n  }\n"
    t += "    .t01 tr:nth-child(even) {\n    background-color:  #eee;\n  }\n"
    t += "    .t01 tr:nth-child(odd) {\n    background-color:  #fff;\n  }\n"
    t += "    .t01 th {\n    background-color: black;\n    color: white;\n    border: 1px solid white;\n  }\n"
    t += "    .t01.alerte {\n    color: red;\n    font-weight: bolder;\n  }\n"
    t += "    .alerte {\n    color: red;\n    font-weight: bolder;\n  }\n"
    t += "  </style>"
    return t

def creer_html(nom_fichier="repartition.html"):
    t = "<!DOCTYPE html>\n<html lang='fr'>\n"
    t += "<head>\n  <meta charset='utf-8'/>\n  <title>Répartion mathématiques</title>\n"
    t += f"{css_sytle()}\n"
    t += "</head>\n"
    t += "<body>\n"
    f = open(nom_fichier, 'w')
    f.write(t)
    return f

def clore_html(f):
    f.write("</body>\n</html>")
    f.close()

def complete_html(f, no_prop, classes, repartition, enseignants):
    nom_classes = list(map(lambda x: x[0], classes))
    horaires_classes = list(map(lambda x: x[1], classes))
    totaux_th_classes = list(map(lambda x: x[2], classes))
    totaux_ef_classes = list(map(lambda x: x[3], classes))
    nb_classes = len(nom_classes)
    nom_profs = list(map(lambda x: x[0], enseignants))
    pond_profs = list(map(lambda x: Decimal(int(x[1]*100))/100, enseignants))
    total_profs = list(map(lambda x: Decimal(int(x[2]*100))/100, enseignants))
    hsa_profs = list(map(lambda x: Decimal(int(x[3]*100))/100, enseignants))
    repartition = list(map(lambda x: str(x[2]) if x[2] else "", repartition))
    divisions_par_prof =[]
    for i in range(len(nom_profs)):
        divisions_par_prof.append(
            repartition[i * len(repartition) // len(nom_profs):(i + 1) * len(repartition) // len(nom_profs)])
    # divisions_par_prof = [divisions[i * len(nom_profs) - 1:i * len(nom_profs) + len(nom_classes)] for i in
    #                       range(1, 1 + len(divisions) // len(nom_classes))]
    total_p = str(sum(total_profs))
    total_pond = str(sum(pond_profs))
    total_hsa = str(sum(hsa_profs))
    t = f"<h1>Proposition n°{no_prop}</h1>\n"
    t += "<table class='t01'>\n  <tr>\n    <th></th>\n    <th>" + '</th>\n    <th>'.join(nom_classes) + \
         "    </th>\n    <th>Total</th>\n    <th>Pondé-ration</th>\n    <th>HSA</th>\n  </tr>\n"
    t += "  <tr>\n    <th>Horaire</th><th>" + "</th><th>".join([str(i) for i in horaires_classes]) + \
         "</th><th></th><th></th><th></th>\n  </tr>"
    t += "  <tr>\n    <th>Nb de classes</th><th>" + "</th><th>".join([str(i) for i in totaux_th_classes]) + \
         "</th><th></th><th></th><th></th>\n  </tr>"
    for ligne in range(len(nom_profs)):
        t += f"  <tr>\n    <td class='nom'>{nom_profs[ligne]}</td>\n    <td>" + \
             "</td><td>".join(divisions_par_prof[ligne]) + \
             f"</td><td>{total_profs[ligne]}</td><td>{pond_profs[ligne]}</td>"
        if hsa_profs[ligne] < 0:
            t += f"<td class='alerte'>{hsa_profs[ligne]}</td>\n"
        else:
            t += f"<td>{hsa_profs[ligne]}</td>\n"
        t += "  </tr>\n"
    t += "  <tr>\n    <th>Totaux</th>"
    for c in range(len(nom_classes)):
        if totaux_th_classes[c] != totaux_ef_classes[c]:
            t += f"<th class='t01 alerte'>{totaux_ef_classes[c]}</th>"
        else:
            t += f"<th>{totaux_ef_classes[c]}</th>"
    t += f"<th>{total_p}</th><th>{total_pond}</th><th>{total_hsa}</th>"
    t += "\n  </tr>"
    t += "</table>\n"
    f.write(t)

