import sqlite3


def creer_tables(con):
    with con:
        con.execute("PRAGMA page_size = 65536;")
        con.execute("DROP TABLE IF EXISTS \"SERVICE\";")
        con.execute("""CREATE TABLE IF NOT EXISTS "SERVICE" (
        "id_service"	INTEGER,
        "id_serv_elem"	INTEGER,
        FOREIGN KEY("id_serv_elem") REFERENCES "SERVICEELEMENTAIRE"("id_serv_elem"),
        PRIMARY KEY("id_service","id_serv_elem")
        );""")
        con.execute("CREATE INDEX index_service ON SERVICE (id_serv_elem,id_service);")
        con.execute("DROP TABLE IF EXISTS \"ENSEIGNANT\";")
        con.execute("""CREATE TABLE IF NOT EXISTS "ENSEIGNANT" (
        "nom"	TEXT NOT NULL,
        "horaire"	NUMERIC NOT NULL,
        "hsa"	NUMERIC NOT NULL,
        "sous_service"	NUMERIC NOT NULL,
        PRIMARY KEY("nom")
        );""")
        con.execute("DROP TABLE IF EXISTS \"SERVICEELEMENTAIRE\";")
        con.execute("""CREATE TABLE IF NOT EXISTS "SERVICEELEMENTAIRE" (
        "id_serv_elem"	INTEGER,
        "id_classe"	INTEGER,
        "nom"	TEXT,
        "nb_divisions_prof"	INTEGER,
        PRIMARY KEY("id_serv_elem" AUTOINCREMENT),
        FOREIGN KEY("nom") REFERENCES "ENSEIGNANT"("nom"),
        FOREIGN KEY("id_classe") REFERENCES "CLASSE"("id_classe")
        );""")
        con.execute("CREATE INDEX index_se_id_classe ON SERVICEELEMENTAIRE (id_classe);")
        con.execute("CREATE INDEX index_se_nom ON SERVICEELEMENTAIRE (nom);")
        con.execute("CREATE INDEX index_se_nb_div ON SERVICEELEMENTAIRE (nb_divisions_prof);")
        con.execute("DROP TABLE IF EXISTS \"FILIERE\";")
        con.execute("""CREATE TABLE IF NOT EXISTS "FILIERE" (
        "filiere"	TEXT NOT NULL,
        "abreviation"	TEXT NOT NULL,
        "ponderation_max"	INTEGER DEFAULT NULL,
        PRIMARY KEY("filiere")
        );""")
        con.execute("DROP TABLE IF EXISTS \"INTITULE\";")
        con.execute("""CREATE TABLE IF NOT EXISTS "INTITULE" (
        "intitule"	TEXT,
        "abreviation"	TEXT NOT NULL,
        "max_intitule"	INTEGER DEFAULT 2,
        PRIMARY KEY("intitule")
        );""")
        con.execute("DROP TABLE IF EXISTS \"NIVEAU\";")
        con.execute("""CREATE TABLE IF NOT EXISTS "NIVEAU" (
        "niveau"	TEXT,
        "abreviation"	TEXT,
        "ponderation"	INTEGER DEFAULT 0,
        PRIMARY KEY("niveau")
        );""")
        con.execute("DROP TABLE IF EXISTS \"PROGRAMME\";")
        con.execute("""CREATE TABLE IF NOT EXISTS "PROGRAMME" (
        "programme"	TEXT,
        "abreviation"	TEXT,
        PRIMARY KEY("programme")
        );""")
        con.execute("DROP TABLE IF EXISTS \"CLASSE\";")
        con.execute("""CREATE TABLE IF NOT EXISTS "CLASSE" (
        "id_classe"	INTEGER,
        "horaire"	NUMERIC NOT NULL,
        "nb_divisons"	INTEGER NOT NULL,
        "programme"	TEXT,
        "niveau"	TEXT,
        "filiere"	TEXT NOT NULL,
        "intitule"	TEXT NOT NULL,
        "max_division"	INTEGER DEFAULT 2,
        PRIMARY KEY("id_classe" AUTOINCREMENT),
        FOREIGN KEY("intitule") REFERENCES "INTITULE"("intitule"),
        FOREIGN KEY("programme") REFERENCES "PROGRAMME"("programme"),
        FOREIGN KEY("niveau") REFERENCES "NIVEAU"("niveau"),
        FOREIGN KEY("filiere") REFERENCES "FILIERE"("filiere")
        );""")
        con.execute("CREATE INDEX index_idc ON CLASSE (id_classe);")
        # con.execute("PRAGMA journal_mode = MEMORY;")
        # con.execute("PRAGMA synchronous = OFF;")
        con.execute("PRAGMA journal_mode = WALL;")
        con.execute("PRAGMA synchronous = NORMAL;")


def remplir_tables(con):
    with con:
        liste = [('Prof 8', 12, 1, -0.5), ('Prof 2', 15, 2.7, -0.15), ('Prof 3', 15, 3, -0.15),
                 ('Stagiaire 1', 8, 1, 0), ('Prof 5', 18, 2, -0.15), ('Prof 6', 15, 2.7, -0.15),
                 ('Prof 1', 15, 2.7, -0.15), ('Prof 4', 15, 3, -0.15), ('Prof 10', 15, 4, -0.15),
                 ('Prof 9', 15, 2.7, -0.15), ('Prof 7', 18, 2, -0.15), ('Stagiaire 2', 8, 1, 0)]
        con.executemany("INSERT INTO 'ENSEIGNANT' VALUES (?, ?, ?, ?);", liste)

        liste = [('générale', 'G', 1), ('technologique', 'Techno', 1), ('générale et technologique', 'GT', 0)]
        con.executemany("INSERT INTO 'FILIERE' VALUES (?, ?, ?);", liste)

        liste = [('Mathématiques', 'math', 2), ('SNT', 'SNT', 2), ('STMG', 'STMG', 1), ('ST2S', 'ST2S', 2),
                 ('Option mathématiques complémentaires', 'math compl.', 2),
                 ('Option mathématiques expertes', 'math expertes', 2), ('Spécialité de mathématiques', 'spé math', 2),
                 ('Spécialité de NSI', 'spé NSI', 2), ('Coopération en spécialité de mathématiques', 'coopération', 2)]
        con.executemany("INSERT INTO 'INTITULE' VALUES (?, ?, ?);", liste)

        liste = [('première', '1re', 0.1), ('seconde', '2de', 0), ('terminale', 'Term', 0.1)]
        con.executemany("INSERT INTO 'NIVEAU' VALUES (?, ?, ?);", liste)

        liste = [('première technologique', '1re techno'), ('mathématiques de seconde', '2de'), ('SNT', 'SNT'),
                 ('première spécialité de mathématiques', '1re spé math'),
                 ('terminale spécialité de mathématiques', 'term spé math'),
                 ('première spécialité de NSI', '1re spé NSI'),
                 ('terminale spécialité de NSI', 'term spé NSI'), ('terminale technologique', 'term techno'),
                 ('terminale option mathématiques complémentaires', 'term option math compl.'),
                 ('terminale option mathématiques expertes', 'term math expertes'), ('', None)]
        con.executemany("INSERT INTO 'PROGRAMME' VALUES (?, ?);", liste)

        liste = [(1, 5, 14, 'mathématiques de seconde', 'seconde', 'générale et technologique', 'Mathématiques', 2),
                 (2, 3.5, 4, 'première technologique', 'première', 'technologique', 'STMG', 1),
                 (3, 3.5, 2, 'première technologique', 'première', 'technologique', 'ST2S', 2),
                 (4, 4, 6, 'première spécialité de mathématiques', 'première', 'générale',
                  'Spécialité de mathématiques', 2),
                 (5, 4, 2, 'première spécialité de NSI', 'première', 'générale', 'Spécialité de NSI', 2),
                 (6, 3.5, 3, 'terminale technologique', 'terminale', 'technologique', 'STMG', 1),
                 (7, 3.5, 2, 'terminale technologique', 'terminale', 'technologique', 'ST2S', 2),
                 (8, 6, 3, 'terminale spécialité de mathématiques', 'terminale', 'générale',
                  'Spécialité de mathématiques', 2),
                 (9, 6, 1, 'terminale spécialité de NSI', 'terminale', 'générale', 'Spécialité de NSI', 2),
                 (10, 3, 2, 'terminale option mathématiques complémentaires', 'terminale', 'générale',
                  'Option mathématiques complémentaires', 2),
                 (11, 3, 2, 'terminale option mathématiques expertes', 'terminale', 'générale',
                  'Option mathématiques expertes', 2),
                 # (12, 0.5, 9, '', 'première', 'générale', 'Coopération en spécialité de mathématiques', 1),
                 ]
        con.executemany("INSERT INTO 'CLASSE' VALUES (?, ?, ?, ?, ?, ?, ?, ?);", liste)


def service_initial(con):
    with con:
        M = con.execute("SELECT max(max_division) FROM CLASSE;").fetchone()[0]
        liste = [(i, i) for i in range(1, M + 1)]
        con.executemany("INSERT INTO SERVICEELEMENTAIRE (id_classe, nom, nb_divisions_prof) SELECT id_classe, nom, ?"
                        "FROM CLASSE CROSS JOIN ENSEIGNANT WHERE max_division >= ? ORDER BY nom;", liste)
        liste = [("Prof 9", 4, 1),  # ("Prof 9", 11, 1), #("Prof 9", 12, 1),
                 ("Prof 10", 4, 1),  # ("Prof 10", 10, 1), #("Prof 10", 12, 2),
                 ("Prof 5", 1, 1), ("Prof 5", 4, 1),  # ("Prof 5", 12, 1),
                 ("Prof 4", 2, 1), ("Prof 4", 3, 1), ("Prof 4", 6, 1), ("Prof 4", 7, 1), ("Prof 4", 10, 1),
                 ("Prof 2", 4, 1),
                 ("Prof 7", 1, 1), ("Prof 7", 3, 1),  # ("Prof 7", 12, 2),
                 ("Prof 6", 8, 1),  # ("Prof 6", 12, 1),
                 ("Prof 1", 8, 1), ("Prof 1", 6, 1),  # ("Prof 1", 12, 1),
                 ("Prof 8", 1, 1), ("Prof 8", 8, 1),  # ("Prof 8", 12, 1),
                 ("Stagiaire 1", 1, 1), ("Stagiaire 1", 2, 1),
                 ("Stagiaire 2", 1, 1), ("Stagiaire 2", 2, 1),
                 ("Prof 3", 5, 2), ("Prof 3", 9, 1)]
        liste = [(e[0], e[1], e[2]) for e in liste]
        con.executemany("INSERT INTO SERVICE SELECT 1, id_serv_elem FROM SERVICEELEMENTAIRE NATURAL JOIN CLASSE "
                        "WHERE nom = ? AND id_classe = ? AND nb_divisions_prof = ?;", liste)


def update_service():
    t = """SELECT id_serv_elem
           FROM   SERVICE
           WHERE  id_service = :service
                  AND id_serv_elem != :classe
           UNION ALL
           SELECT :new_classe
           ORDER BY id_serv_elem"""
    return t


def insert_service():
    t = """SELECT id_serv_elem
           FROM   SERVICE
           WHERE  id_service = :service
           UNION ALL
           SELECT :new_classe
           ORDER BY id_serv_elem"""
    return t


def insert_or_update(con, service, classe, nom):
    t = f"""SELECT
              (SELECT id_serv_elem
               FROM SERVICE
               NATURAL JOIN SERVICEELEMENTAIRE
               WHERE id_service = :service
                 AND id_classe = :classe
                 AND nom = :nom) AS precedent,
                   ifnull(
                            (SELECT id_serv_elem
                             FROM SERVICEELEMENTAIRE
                             WHERE id_classe = :classe
                               AND nom=:nom
                               AND nb_divisions_prof =
                                 (SELECT nb_divisions_prof + 1
                                  FROM SERVICEELEMENTAIRE
                                  WHERE id_serv_elem =
                                      (SELECT id_serv_elem
                                       FROM SERVICE
                                       NATURAL JOIN SERVICEELEMENTAIRE
                                       WHERE id_service = :service AND id_classe = :classe AND nom=:nom))),
                            (SELECT id_serv_elem
                             FROM SERVICEELEMENTAIRE
                             WHERE id_classe = :classe
                               AND nom = :nom
                               AND nb_divisions_prof = 1)) AS suivant
                WHERE suivant IS NOT NULL;"""
    r = con.execute(t, {'service': service, 'classe': classe, 'nom': nom}).fetchone()
    return [service, r[0], r[1]]


def recherche_profs():
    t = f"""SELECT HORAIRES.nom, restant_du_min_new, restant_du_max_new, restant_du_min_old, restant_du_max_old
            FROM
              (SELECT nom,
                      du_min - sum(face_eleve_new) - sum(ponderation_new) AS restant_du_min_new,
                      du_max - sum(face_eleve_new) - sum(ponderation_new) AS restant_du_max_new,
                      sum(nb_prog) AS nb_programmes,
                      du_min - sum(face_eleve_old) - sum(ponderation_old) AS restant_du_min_old,
                      du_max - sum(face_eleve_old) - sum(ponderation_old) AS restant_du_max_old
               FROM
                 (SELECT B.nom,
                         du_min,
                         du_max,
                         min(B.ponderation_max, B.ponderation_theorique_new) AS ponderation_new,
                         face_eleve_new,
                         min(A.ponderation_max, A.ponderation_theorique_old) AS ponderation_old,
                         face_eleve_old,
                         nb_prog
                  FROM
                    (SELECT SERVICEELEMENTAIRE.nom,
                            ENSEIGNANT.horaire + ENSEIGNANT.sous_service AS du_min,
                            ENSEIGNANT.horaire + ENSEIGNANT.hsa AS du_max,
                            ponderation_max,
                            sum(ponderation * CLASSE.horaire * nb_divisions_prof) AS ponderation_theorique_new,
                            sum(CLASSE.horaire * nb_divisions_prof) AS face_eleve_new
                     FROM
                       (SELECT *
                        FROM SERVICE
                        WHERE id_service = :service
                        UNION ALL SELECT *
                        FROM (
                                (SELECT :service AS id_service)
                              CROSS JOIN
                                (SELECT id_serv_elem
                                 FROM SERVICEELEMENTAIRE
                                 NATURAL JOIN CLASSE
                                 WHERE id_classe = :classe
                                   AND nb_divisions_prof = 1)))
                     NATURAL JOIN SERVICEELEMENTAIRE
                     NATURAL JOIN CLASSE
                     NATURAL JOIN FILIERE
                     JOIN NIVEAU ON NIVEAU.niveau = CLASSE.niveau
                     JOIN ENSEIGNANT ON ENSEIGNANT.nom = SERVICEELEMENTAIRE.nom
                     WHERE id_service = :service GROUP  BY SERVICEELEMENTAIRE.nom,
                                                    ponderation_max) B
                  LEFT OUTER JOIN
                    (SELECT SERVICEELEMENTAIRE.nom,
                            ponderation_max,
                            sum(ponderation * CLASSE.horaire * nb_divisions_prof) AS ponderation_theorique_old,
                            sum(CLASSE.horaire * nb_divisions_prof) AS face_eleve_old,
                            count(DISTINCT programme) AS nb_prog
                     FROM SERVICE
                     NATURAL JOIN SERVICEELEMENTAIRE
                     NATURAL JOIN CLASSE
                     NATURAL JOIN FILIERE
                     JOIN NIVEAU ON NIVEAU.niveau = CLASSE.niveau
                     JOIN ENSEIGNANT ON ENSEIGNANT.nom = SERVICEELEMENTAIRE.nom
                     WHERE id_service = :service AND programme != ''
                     GROUP  BY SERVICEELEMENTAIRE.nom,
                                                    ponderation_max) A ON A.nom = B.nom
                  AND A.ponderation_max = B.ponderation_max) GROUP  BY nom
               HAVING nb_programmes <= :NOMBRE_NIVEAUX_MAX
               AND restant_du_max_new >= 0
               ORDER  BY restant_du_min_old) HORAIRES
            JOIN
              (SELECT nom -- Conserve les profs n'ayant pas plus de 2 divisions par niveau
               FROM
                 (SELECT *
                     FROM SERVICE
                     WHERE id_service = :service
                     UNION ALL SELECT *
                     FROM (
                             (SELECT :service AS id_service)
                           CROSS JOIN
                             (SELECT id_serv_elem
                              FROM SERVICEELEMENTAIRE
                              NATURAL JOIN CLASSE
                              WHERE id_classe = :classe
                                AND nb_divisions_prof = 1)))
               NATURAL JOIN SERVICEELEMENTAIRE
               NATURAL JOIN CLASSE
               WHERE id_classe = :classe
               GROUP  BY nom, CLASSE.id_classe
               HAVING sum(nb_divisions_prof) <= max_division) C ON HORAIRES.nom = C.nom
            JOIN
              (SELECT nom -- Conserve les profs n'ayant pas plus d'une STMG
                            FROM(
                            SELECT nom, max_division - count(INTITULE.intitule) AS res
                               FROM
                                    (SELECT *
                                     FROM SERVICE
                                 WHERE id_service = :service
                                     UNION ALL SELECT *
                                     FROM (
                                             (SELECT :service AS id_service)
                                           CROSS JOIN
                                             (SELECT id_serv_elem
                                              FROM SERVICEELEMENTAIRE
                                              NATURAL JOIN CLASSE
                                              WHERE id_classe = :classe
                                                AND nb_divisions_prof = 1)))
                               NATURAL JOIN SERVICEELEMENTAIRE
                               NATURAL JOIN CLASSE
                               JOIN INTITULE ON CLASSE.intitule = INTITULE.intitule
                               GROUP  BY nom, INTITULE.intitule
                            )
                            GROUP BY nom
                            HAVING min(res) >= 0) D ON HORAIRES.nom = D.nom
            JOIN
              (SELECT nom -- conserve les profs ayant au plus 3 services
               FROM
                 (SELECT *
                  FROM SERVICE
                  WHERE id_service = :service
                  UNION ALL SELECT *
                  FROM (
                          (SELECT :service AS id_service)
                        CROSS JOIN
                          (SELECT id_serv_elem
                           FROM SERVICEELEMENTAIRE
                           NATURAL JOIN CLASSE
                           WHERE id_classe = :classe
                             AND nb_divisions_prof = 1)))
               NATURAL JOIN SERVICEELEMENTAIRE
               NATURAL JOIN CLASSE
               WHERE programme != ''
               GROUP BY nom
               HAVING count(DISTINCT programme)<=:NOMBRE_NIVEAUX_MAX) E ON HORAIRES.nom = E.nom;"""
    return t


def recherche_profs_2():
    t = f"""SELECT HORAIRES.nom, restant_du_min_new, restant_du_max_new
            FROM
              (SELECT nom,
                      du_min - sum(face_eleve_new) - sum(ponderation_new) AS restant_du_min_new,
                      du_max - sum(face_eleve_new) - sum(ponderation_new) AS restant_du_max_new,
                      sum(nb_prog) AS nb_programmes
               FROM
                 (SELECT B.nom,
                         du_min,
                         du_max,
                         min(B.ponderation_max, B.ponderation_theorique_new) AS ponderation_new,
                         face_eleve_new,
                         nb_prog
                  FROM
                    (SELECT SERVICEELEMENTAIRE.nom,
                            count(DISTINCT programme) AS nb_prog,
                            ENSEIGNANT.horaire + ENSEIGNANT.sous_service AS du_min,
                            ENSEIGNANT.horaire + ENSEIGNANT.hsa AS du_max,
                            ponderation_max,
                            sum(ponderation * CLASSE.horaire * nb_divisions_prof) AS ponderation_theorique_new,
                            sum(CLASSE.horaire * nb_divisions_prof) AS face_eleve_new
                     FROM
                       (SELECT *
                        FROM SERVICE
                        WHERE id_service = :service
                        UNION ALL SELECT *
                        FROM (
                                (SELECT :service AS id_service)
                              CROSS JOIN
                                (SELECT id_serv_elem
                                 FROM SERVICEELEMENTAIRE
                                 NATURAL JOIN CLASSE
                                 WHERE id_classe = :classe
                                   AND nb_divisions_prof = 1
                                 UNION ALL
                                 SELECT id_serv_elem
                                 FROM SERVICEELEMENTAIRE
                                 NATURAL JOIN CLASSE
                                 WHERE id_classe = :classe_attribuee
                                   AND nb_divisions_prof = 1
                                   AND nom = :nom
                                )))
                     NATURAL JOIN SERVICEELEMENTAIRE
                     NATURAL JOIN CLASSE
                     NATURAL JOIN FILIERE
                     JOIN NIVEAU ON NIVEAU.niveau = CLASSE.niveau
                     JOIN ENSEIGNANT ON ENSEIGNANT.nom = SERVICEELEMENTAIRE.nom
                     WHERE id_service = :service AND programme != ''
                     GROUP  BY SERVICEELEMENTAIRE.nom, ponderation_max) B
                  ) GROUP  BY nom
               HAVING nb_programmes <= :NOMBRE_NIVEAUX_MAX
               AND restant_du_max_new >= 0
               ORDER  BY restant_du_min_new) HORAIRES
            JOIN
              (SELECT nom
               FROM
                 (SELECT *
                     FROM SERVICE
                     WHERE id_service = :service
                     UNION ALL SELECT *
                     FROM (
                             (SELECT :service AS id_service)
                           CROSS JOIN
                             (SELECT id_serv_elem
                              FROM SERVICEELEMENTAIRE
                              NATURAL JOIN CLASSE
                              WHERE id_classe = :classe
                                AND nb_divisions_prof = 1
                                                     UNION ALL
                                 SELECT id_serv_elem
                                 FROM SERVICEELEMENTAIRE
                                 NATURAL JOIN CLASSE
                                 WHERE id_classe = :classe_attribuee
                                   AND nb_divisions_prof = 1
                                   AND nom = :nom
                           )))
               NATURAL JOIN SERVICEELEMENTAIRE
               NATURAL JOIN CLASSE
               WHERE id_classe = :classe
               GROUP  BY nom, CLASSE.id_classe
               HAVING sum(nb_divisions_prof) <= max_division) C ON HORAIRES.nom = C.nom
            JOIN
              (SELECT nom -- Conserve les profs n'ayant pas plus d'une STMG
                FROM(
                SELECT nom, max_division - count(INTITULE.intitule) AS res
                   FROM
                        (SELECT *
                         FROM SERVICE
                     WHERE id_service = :service
                         UNION ALL SELECT *
                         FROM (
                                 (SELECT :service AS id_service)
                               CROSS JOIN
                                 (SELECT id_serv_elem
                                  FROM SERVICEELEMENTAIRE
                                  NATURAL JOIN CLASSE
                                  WHERE id_classe = :classe
                                    AND nb_divisions_prof = 1)))
                   NATURAL JOIN SERVICEELEMENTAIRE
                   NATURAL JOIN CLASSE
                   JOIN INTITULE ON CLASSE.intitule = INTITULE.intitule
                   GROUP  BY nom, INTITULE.intitule
                )
                GROUP BY nom
                HAVING min(res) >= 0) D ON HORAIRES.nom = D.nom
            JOIN
              (SELECT nom
               FROM
                 (SELECT *
                  FROM SERVICE
                  WHERE id_service = :service
                  UNION ALL SELECT *
                  FROM (
                          (SELECT :service AS id_service)
                        CROSS JOIN
                          (SELECT id_serv_elem
                           FROM SERVICEELEMENTAIRE
                           NATURAL JOIN CLASSE
                           WHERE id_classe = :classe
                             AND nb_divisions_prof = 1
                                                  UNION ALL
                                 SELECT id_serv_elem
                                 FROM SERVICEELEMENTAIRE
                                 NATURAL JOIN CLASSE
                                 WHERE id_classe = :classe_attribuee
                                   AND nb_divisions_prof = 1
                                   AND nom = :nom
                           )))
               NATURAL JOIN SERVICEELEMENTAIRE
               NATURAL JOIN CLASSE
               WHERE programme != ''
               GROUP BY nom
               HAVING count(DISTINCT programme)<=:NOMBRE_NIVEAUX_MAX) E ON HORAIRES.nom = E.nom"""
    return t


def recherche_enseignant_service_incomplet():
    t = f"""SELECT nom
            FROM   (SELECT nom,
                           du_min - sum(face_eleve) - sum(ponderation) AS restant_du_min
                    FROM   (SELECT B.nom,
                                   du_min,
                                   min(B.ponderation_max, B.ponderation_theorique) AS
                                   ponderation,
                                   face_eleve
                            FROM   (SELECT SERVICEELEMENTAIRE.nom,
                                           ENSEIGNANT.horaire
                                           + ENSEIGNANT.sous_service
                                           AS
                                           du_min,
                                           ponderation_max,
                                           sum(ponderation * CLASSE.horaire *
                                               nb_divisions_prof) AS
                                                   ponderation_theorique,
                                           sum(CLASSE.horaire * nb_divisions_prof)
                                           AS
                                           face_eleve
                                    FROM   (SELECT *
                                            FROM   SERVICE
                                            WHERE  id_service = :service
                                            UNION ALL
                                            SELECT *
                                            FROM   ( (SELECT :service)
                                                     CROSS JOIN (SELECT id_serv_elem
                                                                 FROM   SERVICEELEMENTAIRE
                                                                        NATURAL JOIN CLASSE
                                                                 WHERE
                                                     id_classe = :classe
                                                     AND
                                                     nb_divisions_prof = 1
                                                     AND
                                                     nom = :nom
                                                                ) ))
                                           NATURAL JOIN SERVICEELEMENTAIRE
                                           NATURAL JOIN CLASSE
                                           NATURAL JOIN FILIERE
                                           JOIN NIVEAU
                                             ON NIVEAU.niveau = CLASSE.niveau
                                           JOIN ENSEIGNANT
                                             ON ENSEIGNANT.nom = SERVICEELEMENTAIRE.nom
                                    WHERE  id_service = :service
                                    GROUP  BY SERVICEELEMENTAIRE.nom,
                                              ponderation_max) B)
                    GROUP  BY nom)
            WHERE  restant_du_min > 0;"""
    return t


def services_maximaux():
    t = """ SELECT id_service
            FROM   (SELECT id_service,
                           sum(nb_divisions_prof * CLASSE.horaire) AS total
                    FROM   SERVICE
                           NATURAL JOIN SERVICEELEMENTAIRE
                           NATURAL JOIN CLASSE
                    GROUP  BY id_service)
                   CROSS JOIN (SELECT max(total_service) AS maxi
                               FROM   (SELECT sum(nb_divisions_prof * CLASSE.horaire) AS
                                              total_service
                                       FROM   SERVICE
                                              NATURAL JOIN SERVICEELEMENTAIRE
                                              NATURAL JOIN CLASSE
                                       GROUP  BY id_service))
            WHERE  total = maxi;"""
    return t


def liste_classes():
    t = """ SELECT NIVEAU.abreviation
                   || " "
                   || INTITULE.abreviation AS classe,
                   horaire,
                   nb_divisons,
                   sum(nb_divisions_prof)
            FROM   SERVICE
                   NATURAL JOIN SERVICEELEMENTAIRE
                   NATURAL JOIN CLASSE
                   JOIN INTITULE
                     ON INTITULE.intitule = CLASSE.intitule
                   JOIN NIVEAU
                     ON NIVEAU.niveau = CLASSE.niveau
            WHERE  id_service = :service
            GROUP  BY id_classe
            ORDER  BY id_classe;"""
    return t


def repartition():
    t = """ SELECT E.nom,
                   C.classe,
                   nb_divisions_prof
            FROM   (SELECT DISTINCT nom
                    FROM   SERVICE
                           NATURAL JOIN SERVICEELEMENTAIRE
                           NATURAL JOIN CLASSE
                    WHERE  id_service = :service
                    ORDER  BY nom) E
                   CROSS JOIN (SELECT DISTINCT id_classe,
                                               NIVEAU.abreviation
                                               || ""
                                               || INTITULE.abreviation AS classe
                               FROM   SERVICE
                                      NATURAL JOIN SERVICEELEMENTAIRE
                                      NATURAL JOIN CLASSE
                                      JOIN INTITULE
                                        ON INTITULE.intitule = CLASSE.intitule
                                      JOIN NIVEAU
                                        ON NIVEAU.niveau = CLASSE.niveau
                               WHERE  id_service = :service
                               ORDER  BY id_classe) C
                              LEFT JOIN (SELECT id_classe,
                                                nom,
                                                nb_divisions_prof
                                         FROM   SERVICE
                                                NATURAL JOIN SERVICEELEMENTAIRE
                                         WHERE  id_service = :service) S
                                     ON S.id_classe = C.id_classe
                                        AND S.nom = E.nom;"""
    return t


def services_enseignants():
    t = """ SELECT nom,
                   ponderation,
                   face_eleve + ponderation           AS total,
                   face_eleve + ponderation - horaire AS hsa
            FROM   (SELECT ENSEIGNANT.nom,
                           Count(DISTINCT PROGRAMME.abreviation)   AS niveaux,
                           ENSEIGNANT.horaire,
                           ENSEIGNANT.hsa,
                           ENSEIGNANT.sous_service,
                           Sum(nb_divisions_prof * CLASSE.horaire) AS face_eleve
                    FROM   SERVICE
                           natural JOIN SERVICEELEMENTAIRE
                           natural JOIN CLASSE
                           JOIN ENSEIGNANT
                             ON SERVICEELEMENTAIRE.nom = ENSEIGNANT.nom
                           JOIN PROGRAMME
                             ON CLASSE.programme = PROGRAMME.programme
                    WHERE  id_service = :service
                    GROUP  BY ENSEIGNANT.nom)
                   natural JOIN (SELECT nom,
                                        Sum(pond) AS ponderation
                                 FROM   (SELECT ENSEIGNANT.nom,
                                                ponderation_max,
                                                Min(ponderation_max, Sum(
                                                nb_divisions_prof * CLASSE.horaire *
                                                                     NIVEAU.ponderation)) AS
                                                pond
                                         FROM   SERVICE
                                                natural JOIN SERVICEELEMENTAIRE
                                                natural JOIN CLASSE
                                                natural JOIN NIVEAU
                                                JOIN ENSEIGNANT
                                                  ON SERVICEELEMENTAIRE.nom = ENSEIGNANT.nom
                                                JOIN FILIERE
                                                  ON CLASSE.filiere = FILIERE.filiere
                                         WHERE  id_service = :service
                                         GROUP  BY ENSEIGNANT.nom,
                                                   ponderation_max)
                                 GROUP  BY nom)
            GROUP  BY nom
            ORDER  BY nom;"""
    return t


def classes_a_placer():
    t = """ SELECT A.id_classe, ifnull(restantes, nb_divisions)
            FROM  ( (SELECT id_classe,
                          CLASSE.nb_divisons AS nb_divisions
                   FROM   CLASSE) A
                    LEFT JOIN (SELECT CLASSE.id_classe,
                                      nb_divisons - sum(nb_divisions_prof) AS restantes
                               FROM   SERVICE
                                      NATURAL JOIN SERVICEELEMENTAIRE
                                      NATURAL JOIN CLASSE
                               WHERE  id_service = :service
                               GROUP  BY id_classe) B
                           ON A.id_classe = B.id_classe)
            WHERE  restantes > 0
                    OR restantes IS NULL;"""
    return t


def horaire_a_placer():
    t = """SELECT id_service,
                   max(p),
                   t
            FROM  (SELECT id_service,
                          sum(placee * A.horaire)       AS p,
                          sum(nb_divisions * A.horaire) AS t
                   FROM   ( (SELECT id_classe,
                                  CLASSE.nb_divisons AS nb_divisions,
                                  CLASSE.horaire
                           FROM   CLASSE) A
                            LEFT JOIN (SELECT CLASSE.id_classe,
                                              sum(nb_divisions_prof) AS placee,
                                              id_service
                                       FROM   SERVICE
                                              NATURAL JOIN SERVICEELEMENTAIRE
                                              NATURAL JOIN CLASSE
                                       GROUP  BY id_classe,
                                                 id_service) B
                                   ON A.id_classe = B.id_classe)
                   GROUP  BY id_service);"""
    return t


def init(db):
    con = sqlite3.connect(db)
    creer_tables(con)
    remplir_tables(con)
    service_initial(con)
    con.close()
