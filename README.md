# Repartition
## _Présentation_
__Repartition__  est un programme __Python__ permettant de réaliser la répartition de services d'une équipe d'enseignants.

C'est un projet sur lequel j'ai travaillé après avoir découvert la programmation dynamique lors de la formation au DIU « enseignement de l'informatique au lycée ».

Ce programme utilise le module _multiprocessing_ de Python afin d'utiliser les multiples processeurs des ordinateurs. Le gain de temps est substantiel.

Les services sont stockés dans une base __SQLite__ et les services possibles sont stockés dans un fichier html.

La configuration se fait dans le fichier gestionBaseSQL.py. Il faut modifier les informations dans remplir_tables() et service_initial(). Il serait intéressant d'avoir une interface graphique pour paramétrer l'ensemble, mais je n'ai pas prévu d'en créer une.

## _Exemple_
Pour obtenir les services, on lance la commande `python3 repartition.py`.
```
jerome @ home  ~/projets/repartition
└─ $ ▶ python3 repartition.py 
 30%|████████████▌                            | 104/341 [02:58<26:13,  6.64s/it]
```

On obtient alors deux fichiers `repartition.db` et `repartition.html`.

![Résultat](./images/fichier_html.png)

## _Paramétrage_
### Dans `remplir_table()`
La table `ENSEIGNANT` contient les informations concernant les enseignants de l'équipe.

Dans l'ordre, on trouve : 
1. le nom
2. le service à faire (18 h pour un certifié, 15 h pour un agrégé, ...)
3. le maximum d'HSA accepté par l'enseignant
4. le maximum de sous-service acceptable par l'enseignant. 

La table `FILIERE` contient les différents filières de l'établissement.

Dans l'ordre, on trouve :
1. le nom de la filière
2. le nom abrégé de cette filière
3. le maximum de pondération que cette filière peut offrir

La table `INTITULE` contient les intitulés des formations.

Dans l'ordre, on trouve :
1. le nom de la formation
2. le nom abrégé de cette formation
3. le nombre maximal de classes proposable pour cette formation (personne ne veut se retrouver avec quatre classes de seconde)

La table `NIVEAU` contient les niveaux des formations.

Dans l'ordre, on trouve :
1. le nom du niveau
2. le nom abrégé de ce niveau
3. le niveau de pondération offert avec ce niveau (10 %, soit 0,1 en premières et terminales)

La table `PROGRAMME` contient les programmes enseignés dans les formations.
Cela permet de spécifier que 1re ST2S ou 1re STMG, ce sont les mêmes programmes.
En prenant une classe de chaque, on ne multiplie pas les préparations.

Dans l'ordre, on trouve :
1. le nom du programme
2. le nom abrégé de ce programme

La table `CLASSE` contient les classes de l'établissement.
Il faut faire le lien avec les informations précédentes.

Dans l'ordre, on trouve :
1. le n° d'identification de la classe (il doit être unique)
2. le nombre d'heures d'enseignement (prof) de la classe
3. le nombre de divisions pour cette classe
4. le `programme` de la table précédente
5. le `niveau` de la table précédente
6. la `filière` de la table précédente
7. l'`intitulé` de la table précédente
8. le nombre maximum de division par enseignant

### Dans `service_initial()`
Il faut saisir la liste des vœux des enseignants en donnant dans l'ordre :
1. le nom de l'enseignant renseigné dans la table `ENSEIGNANT`
2. le n° d'identification de la classe renseigné dans la table `CLASSE`
3. le nombre de divisions voulu

Ces données seront respectées.
