import sqlite3
from functools import partial
from multiprocessing import Pool, cpu_count

from tqdm import tqdm

import affichage
import gestionBaseSQL


NOMBRE_NIVEAUX_MAX = 3


def test_service_viable(conn, service: int, classe: int, nom: str, classes_disponibles: list,
                        restant_du_min_new: float) -> bool:
    """
    Vérifie s'il est possible de ne laisser aucun prof en sous service en une étape :
    par exemple, il ne peut y avoir un e certifié à 17 h à qui on ne pourrait proposer que des cours de plus de 3 h,
    ce qui lui ferait plus de 2 hsa.
    Si on n'a pas inséré la dernière classe d'un groupe, il suffit de tester le service du prof courant.
    Dans le cas contraire, il faut tester les services de tous les profs

    Args:
        service: dictionnaire contenant le service de chaque :py:class:`Enseignant`
        classe: classe à ajouter à un service

    Returns:
        Vrai si le service ainsi proposé semble possible
    """
    cd = []
    for c in classes_disponibles:
        if c[0] == classe:
            if c[1] > 1:
                cd.append(c[0])
        else:
            cd.append(c[0])
    if not cd:
        # Toutes les classes sont placées !
        return True
    if len(cd) == len(classes_disponibles):
        # il reste des exemplaires de id_classe disponibles. Il suffit de vérifier que le service de nom est complétable
        if restant_du_min_new <= 0:
            # on peut attribuer cette classe
            return True
        for c in cd:
            horaires = conn.execute(gestionBaseSQL.recherche_profs_2(),
                                    {'service': service, 'classe': classe, 'NOMBRE_NIVEAUX_MAX': NOMBRE_NIVEAUX_MAX,
                                     'classe_attribuee': c, 'nom': nom}).fetchall()
            if nom in [h[0] for h in horaires]:
                return True
    else:
        le = conn.execute(gestionBaseSQL.recherche_enseignant_service_incomplet(),
                          {'service': service, 'nom': nom, 'classe': classe}).fetchall()
        le = [e[0] for e in le]
        for c in cd:
            horaires = conn.execute(gestionBaseSQL.recherche_profs_2(),
                                    {'service': service, 'classe': classe, 'classe_attribuee': c,
                                     'NOMBRE_NIVEAUX_MAX': NOMBRE_NIVEAUX_MAX, 'nom': nom}).fetchall()
            for h in horaires:
                try:
                    le.remove(h[0])
                    if not le:
                        return True
                except ValueError:
                    pass
    return False


def choix_des_profs(conn, service, classe) -> list:
    # def choix_des_profs(t) -> list:
    """
    Renvoie à quel enseignant une classe peut être attribuée pour respecter les horaires, en tenant compte des
     pondérations. Vérifie si la classe n'est pas déjà attribuée à quelqu'un.

    Args:
        service: Le service à amender
        classe: La classe à ajouter

    Returns:
        Une liste de services envisageables
    """
    resultat = []
    classes_disponibles = conn.execute(gestionBaseSQL.classes_a_placer(), {'service': service}).fetchall()
    if not classe in [c[0] for c in classes_disponibles]:
        conn.close()
        return []
    horaires = conn.execute(gestionBaseSQL.recherche_profs(),
                            {'service': service, 'classe': classe, 'NOMBRE_NIVEAUX_MAX': NOMBRE_NIVEAUX_MAX}).fetchall()
    # Calcule l'horaire total et la pondération en ajoutant la classe en cours
    for r in horaires:
        nom, restant_du_min_new, restant_du_max_new, restant_du_min_old, restant_du_max_old = r
        if restant_du_min_old <= 0 <= restant_du_max_new:
            # Service déjà complet mais qui peut êter encore complété en HSA
            res = gestionBaseSQL.insert_or_update(conn, service, classe, nom)
            if res and test_service_viable(conn, service, classe, nom, classes_disponibles, restant_du_min_new):
                resultat.append(res)
        elif restant_du_max_new >= 0:
            # Service incomplet qui peut être complété par cette classe
            res = gestionBaseSQL.insert_or_update(conn, service, classe, nom)
            if res and test_service_viable(conn, service, classe, nom, classes_disponibles, restant_du_min_new):
                resultat.append(res)
                conn.close()
                return resultat
    conn.close()
    return resultat


def sous_processus(services_et_classes: list, dbpath) -> (list, str):
    """
    Opération élémentaire utilisée dans le cadre du multiprocessing.

    Args:
        services_et_classes: Les services à amender

    Returns:
        Les propositions de services et le niveau à éventuellement ne plus traiter sur cette ligne.
    """
    conn = sqlite3.connect(dbpath, timeout=20)
    service, classe = services_et_classes
    s = choix_des_profs(conn, service, classe)
    conn.close()
    if s:
        return s


# @timeit
def traite_un_horaire(classes: list, heure: int, id_s: int, dbpath) -> int:
    """
    Pour chaque situation initiale, ajoute la classe au premier professeur qui peut la prendre dans son service.
    Utilisation d'un seul processeur.

    Args:
        classes: La classe à ajouter au service
        heure: double du nombre d'heures placées actuellement
        id_s: l'id_service du service obtenu

    Returns:
        Les propositions de services et le niveau à éventuellement ne plus traiter sur cette ligne.
    """
    liste_services = []
    conn = sqlite3.connect(dbpath)
    with conn:
        for j in range(len(classes)):
            horaire_classe = conn.execute(f"SELECT horaire FROM CLASSE WHERE id_classe = {classes[j]}").fetchone()[0]
            services = conn.execute(f"""SELECT id_service FROM SERVICE NATURAL JOIN SERVICEELEMENTAIRE
                    NATURAL JOIN CLASSE GROUP BY id_service
                    HAVING sum(nb_divisions_prof * horaire) = {heure / 2 - horaire_classe};""").fetchall()
            # Récupère toutes les propositions qui existaient classe.horaire avant pour une classe donnée afin de
            # les amender
            if services:
                liste_services.extend([(s[0], classes[j]) for s in services])
    conn.close()
    pool = Pool(cpu_count())
    func = partial(sous_processus, dbpath=dbpath)
    mapped = pool.imap_unordered(func, liste_services)
    resultat = []
    for v in mapped:
        if v:
            resultat.extend(v)
    pool.close()
    pool.join()
    nv_services = set()
    conn = sqlite3.connect(dbpath)
    for e in resultat:
        if e[1]:
            nv_services.add(tuple(conn.execute(gestionBaseSQL.update_service(),
                                               {'service': e[0], 'classe': e[1], 'new_classe': e[2]}).fetchall()))
        else:
            nv_services.add(
                tuple(conn.execute(gestionBaseSQL.insert_service(), {'service': e[0], 'new_classe': e[2]}).fetchall()))
    if nv_services:
        les_services = []
        for s in nv_services:
            les_services.extend([(id_s, i[0]) for i in s])
            id_s += 1
        insertion_base(les_services, conn)
    conn.close()
    return id_s


def insertion_base(services, con):
    with con:
        con.executemany("INSERT INTO SERVICE VALUES (?, ?);", services)


def creer_repartition(dbpath):
    """
    Crée les répartitions possibles en fonctions des classes et enseignants sélectionnés, en respectant un certain
    nombre de contraintes : Pas plus de NOMBRE_NIVEAU_MAX par prof, pas plus de NOMBRE_CLASSES_PAR_NIVEAU classe par
    niveau pour un enseignant donné.
    Utilise la programmation dynamique.
    Développé suite à la formation pour l'obtention du DIU NSI.
    Stocke l'ensemble des étapes dans le fichier table.txt

    Returns:
        None
    """
    """Crée les répartitions de service possibles"""
    conn = sqlite3.connect(dbpath)
    with conn:
        id_s, en_place, a_placer = conn.execute(gestionBaseSQL.horaire_a_placer()).fetchone()
        horaire_max_de_classe = int(2 * conn.execute("SELECT max(horaire) FROM   CLASSE;").fetchone()[0])
        classes = conn.execute(gestionBaseSQL.classes_a_placer(), {'service': id_s}).fetchall()
    conn.close()
    classes = tuple([i[0] for i in classes])
    id_s += 1
    for heure in tqdm(range(int(2 * en_place + 1), int(2 * (en_place + a_placer) + 1))):
        id_s = traite_un_horaire(classes, heure, id_s, dbpath)
        if (heure - horaire_max_de_classe) / 2 + 0.5 >= int(2 * en_place + 1):
            conn = sqlite3.connect(dbpath)
            with conn:
                conn.execute("""DELETE FROM SERVICE WHERE id_service IN
            (SELECT id_service FROM SERVICE NATURAL JOIN SERVICEELEMENTAIRE NATURAL JOIN CLASSE GROUP BY id_service
            HAVING sum(horaire * nb_divisions_prof) = :h);""", {'h': (heure - horaire_max_de_classe) / 2 + 0.5})
            conn.close()


def export_html(dbpath, services=[]):
    f = affichage.creer_html()
    conn = sqlite3.connect(dbpath)
    if not services:
        with conn:
            services = conn.execute(gestionBaseSQL.services_maximaux()).fetchall()
    for i, v in enumerate(services):
        with conn:
            classes = conn.execute(gestionBaseSQL.liste_classes(), {'service': v[0]}).fetchall()
            repartition = conn.execute(gestionBaseSQL.repartition(), {'service': v[0]}).fetchall()
            enseignants = conn.execute(gestionBaseSQL.services_enseignants(), {'service': v[0]}).fetchall()
        affichage.complete_html(f, i + 1, classes, repartition, enseignants)
    affichage.clore_html(f)
    conn.close()


if __name__ == '__main__':
    db = 'repartition.db'
    gestionBaseSQL.init(db)
    creer_repartition(db)
    export_html(db)
